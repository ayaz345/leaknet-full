sub CreateFile
{
	local( $filename ) = shift;
	local( *FILE );

	open FILE, ">$filename";
	close FILE;
}

sub ProcessFile
{
	local( $filename ) = shift;
	local( @fileContents );
#	print "$filename\n";
	if( $filename =~ /\.vtf/i )
	{
		$filename =~ s/$texturecomparedir/$texturebuilddir/gi;
		$filename =~ s/materials/materialsrc/gi;
		$filename =~ s/\.vtf/\.tga/gi;
		local( $cmd ) = "$texturebuilddir\\bin\\vtex -quiet -mkdir $filename";
		print $cmd . "\n";
		system $cmd;
	}
}

sub ProcessDir
{
	local( $dir ) = shift;
	$dir =~ s/$texturecomparedir/$texturebuilddir/gi;
	mkdir $dir;
}

sub ProcessFileOrDirectory
{
	local( $name ) = shift;
#	print "$name\n";

#	If the file has "." at the end, skip it.
	if( $name eq "." || $name eq ".." || $name =~ /\.$/ )
	{
#		print "skipping: $name\n";
		return;
	}

#   Figure out if it's a file or a directory.
	if( -d $name )
	{
		ProcessDir( $name );
		local( *SRCDIR );
		opendir SRCDIR, $name;
		local( @dir ) = readdir SRCDIR;
		closedir SRCDIR;

		local( $item );
		while( $item = shift @dir )
		{
			&ProcessFileOrDirectory( $name . "/" . $item );
		}
	}
	elsif( -f $name )
	{
		&ProcessFile( $name );
	}
	else
	{
		print "$name is neither a file or a directory\n";
	}
	return;
}

$texturebuilddir		= shift;
$texturecomparedir 		= shift;
$texturebuilddir =~ s,\\,/,g;
$texturecomparedir =~ s,\\,/,g;

if( !$texturebuilddir || !$texturecomparedir )
{
	die "Usage: buildalltextures.pl texturebuilddir texturecomparedir";
}

print "\$texturecomparedir = \"$texturecomparedir\"\n";
print "\$texturebuilddir = \"$texturebuilddir\"\n";

opendir SRCDIR, $texturecomparedir;
@dir = readdir SRCDIR;
closedir SRCDIR;

while( $item = shift @dir )
{
	&ProcessFileOrDirectory( "$texturecomparedir/$item" );
}


