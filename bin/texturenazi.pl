# todo:
# what do to about _height tgas?
# bitch about _height.vtf?

$materialsrc_dir = "\$/hl2/materialsrc";
$materials_dir = "\$/hl2/release/dev/hl2/materials";

sub PadNumber
{
	local( $str ) = shift;
	local( $desiredLen ) = shift;
	local( $len ) = length $str;
	while( $len < $desiredLen )
	{
		$str = "0" . $str;
		$len++;
	}
	return $str;
}

sub SSGetFileToArray
{
	# vss command line is really stupid, so to get it to cat the file instead of running notepad, we rename
	# cat.exe to notepad.exe in a subdir.
	$file = shift;
	chdir "hack";
	#print STDERR $file . "\n";
	local( @shit ) = `ss view \"$file\"`;
	chdir "..";
	return @shit;
}

@srcdir = `ssdir.pl \"$materialsrc_dir\"`;

foreach $file (@srcdir)
{
	$file =~ s/\n//;
	$file =~ tr/[A-Z]/[a-z]/;
	$origname = $file;
	$cmd = "\$file =~ s,\\$materialsrc_dir/,,i;";
	eval $cmd;
	if( $file =~ s/\.tga//i )
	{
		$tga{$file} = $origname;
	}
	elsif( $file =~ s/\.txt//i )
	{
		$txt{$file} = $origname;
	}
	else
	{
		print "doesn't belong: $origname\n";
	}
}

@dstdir = `ssdir.pl \"$materials_dir\"`;

foreach $file (@dstdir)
{
	$file =~ s/\n//;
	$file =~ tr/[A-Z]/[a-z]/;
	$origname = $file;
	$cmd = "\$file =~ s,\\$materials_dir/,,i;";
	eval $cmd;
	if( $file =~ s/\.vtf//i )
	{
		$vtf{$file} = $origname;
	}
	elsif( $file =~ s/\.vmt//i )
	{
	}
	elsif( $file =~ s/\.psh//i )
	{
	}
	else
	{
		print "doesn't belong: $origname\n";
	}
}

foreach $txtfile ( sort( keys( %txt ) ) )
{
#	print "blah: $txt{$txtfile}\n";
	@tmp = &SSGetFileToArray( $txt{$txtfile} );
#	print @tmp;
	foreach $line (@tmp)
	{
		if( $line =~ /\"startframe\"\s+\"(\d+)\"/i )
		{
			$startframe{$txtfile} = $1;
#			print "\$startframe\{$txtfile\} = $startframe{$txtfile}\n";
		}
		elsif( $line =~ /\"endframe\"\s+\"(\d+)\"/i )
		{
			$endframe{$txtfile} = $1;
#			print "\$endframe\{$txtfile\} = $endframe{$txtfile}\n";
		}
	}	
}

#foreach $tgafile ( sort( keys( %tga ) ) )
#{
#	if( !defined( $vtf{$tgafile} ) )
#	{
#		print "Extra tga: $tga{$tgafile}\n";
#	}
#}

foreach $vtffile ( sort( keys( %vtf ) ) )
{
#	print "blah: $vtffile\n";
	if( defined( $startframe{$vtffile} ) && defined( $endframe{$vtffile} ) )
	{
		# animated texture
		for( $i = $startframe{$vtffile}; $i <= $endframe{$vtffile}; $i++ )
		{
			$frametganame = $vtffile . &PadNumber( $i, 3 );
			if( !defined( $tga{$frametganame} ) )
			{
				printf "missing animated tga: $materialsrc_dir/" . $frametganame . ".tga\n";
			}
		}
	}
	elsif( !defined( $tga{$vtffile} ) )
	{
		# texture missing. . check to see if it's a cubemap.
		if( !defined( $tga{$vtffile . "bk"} ) ||
		    !defined( $tga{$vtffile . "dn"} ) ||
		    !defined( $tga{$vtffile . "ft"} ) ||
		    !defined( $tga{$vtffile . "lf"} ) ||
		    !defined( $tga{$vtffile . "rt"} ) ||
		    !defined( $tga{$vtffile . "up"} ) )
		{
			print "missing tga: $materialsrc_dir/" . "$vtffile\.tga\n";
		}
			
	}
}

