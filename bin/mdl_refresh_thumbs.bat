@echo off

set GAMEROOT=u:\dev
set MDLROOT=%GAMEROOT%\%1\models
set MATROOT=%GAMEROOT%\%1\materials
set MDLLOGFILE=%GAMEROOT%\bin\refresh_mdl_thumbs.log
set VPROJECT=%GAMEROOT%\%1


REM Delete the local models and materials trees, CTRL-C skips
@echo Delete the local models and materials trees
@echo Delete the local models and materials trees > %MDLLOGFILE%
@echo ---------------------------------------------- >> %MDLLOGFILE%
pause
rmdir %MDLROOT% /s /q
rmdir %MATROOT% /s /q
md %MDLROOT%
md %MATROOT%
cd %GAMEROOT%
:SkipDelete


@echo Get the latest content
@echo Get the latest content >> %MDLLOGFILE%
@echo ---------------------------------------------- >> %MDLLOGFILE%
pause
ss Cp $/%1/release/dev
ss Decloak $/%1/release/dev/%1/materials
ss Decloak $/%1/release/dev/%1/models
ss Get * -R -Gck >> %MDLLOGFILE%
ss Cloak $/%1/release/dev/%1/materials
ss Cloak $/%1/release/dev/%1/models
:SkipGetLatest


@echo - >> %MDLLOGFILE%
@echo Check out all the JPEGs
@echo Check out all the JPEGs >> %MDLLOGFILE%
@echo ---------------------------------------------- >> %MDLLOGFILE%
pause
ss Checkout $/%1/release/dev/%1/models/*.jpg -R -C- -I- >> %MDLLOGFILE%
:SkipCheckout


@echo - >> %MDLLOGFILE%
@echo Build new JPEGs
@echo Build new JPEGs >> %MDLLOGFILE%
@echo ---------------------------------------------- >> %MDLLOGFILE%
pause
perl %GAMEROOT%\bin\mdlupdatethumbs.pl %MDLROOT% >> %MDLLOGFILE%
:SkipBuild

@echo - >> %MDLLOGFILE%
@echo Check in all the JPEGs
@echo Check in all the JPEGs >> %MDLLOGFILE%
@echo ---------------------------------------------- >> %MDLLOGFILE%
pause
ss Checkin $/%1/release/dev/%1/models/*.jpg -R -C- -I- >> %MDLLOGFILE%
:SkipCheckin


REM FIXME: This sourcesafe stuff doesn't work!
REM @echo - >> %MDLLOGFILE%
REM @echo Add any JPEGs that were missing
REM @echo Add any JPEGs that were missing >> %MDLLOGFILE%
REM @echo ---------------------------------------------- >> %MDLLOGFILE%
REM pause
REM ss Cp $/%1/release/dev/%1/models >> %MDLLOGFILE%
REM ss WorkFold $/%1/release/dev/%1/models %MDLROOT% >> %MDLLOGFILE%
REM ss Diff $/%1/release/dev/%1/models -R -C- -F -I- >> %MDLLOGFILE%
:SkipAddMissing

@echo Don't forget to Show Project Differences and add new JPGs!
@echo ---------------------------------------------- >> %MDLLOGFILE%
pause
notepad %MDLLOGFILE%

:end
