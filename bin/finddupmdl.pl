

sub ProcessQCFile
{
	local( $filename ) = shift;
	local( *FILE );
	open FILE, "<$filename";

	while( <FILE> )
	{
		if( m/\$modelname\s+(\S+)\s*$/ )
		{
			if( defined( $alreadyseen{$1} ) )
			{
				print "model $1:\n\t$alreadyseen{$1}\n\t$filename\n";
			}
			$alreadyseen{$1} = $filename;
		}
	}

	close FILE;
}

sub ProcessFile
{
	local( $filename ) = shift;
	local( $isdir ) = -d $filename;
	if( $isdir )
	{
		ProcessDir( $filename );
		return;
	}

	if( $filename =~ m/\.qc$/i )
	{
		ProcessQCFile( $filename );
	}
}

sub ProcessDir
{
	local( $dirname ) = shift;
	if( $dirname =~ m/\/\.$/ ||		# "."
		$dirname =~ m/\/\.\.$/ )	# ".."
	{
		return;
	}
	local( *SRCDIR );
	opendir SRCDIR, $dirname;
	local( @dir ) = readdir SRCDIR;
	closedir SRCDIR;

	local( $item );
	while( $item = shift @dir )
	{
		&ProcessFile( $dirname . "/" . $item );
	}
}

$basedir = shift;
ProcessDir( $basedir );
