sub RemoveFileName
{
	local( $in ) = shift;
	$in = &BackSlashToForwardSlash( $in );
	$in =~ s,/[^/]*$,,;
	return $in;
}

sub RemovePath
{
	local( $in ) = shift;
	$in = &BackSlashToForwardSlash( $in );
	$in =~ s,^(.*)/([^/]*)$,$2,;
	return $in;
}


sub BackSlashToForwardSlash
{
	local( $in ) = shift;
	local( $out ) = $in;
	$out =~ s,\\,/,g;
	return $out;
}


sub MakeDirHier
{
	local( $in ) = shift;
#	print "MakeDirHier( $in )\n";
	$in = &BackSlashToForwardSlash( $in );
	local( @path );
	while( $in =~ m,/, ) # while $in still has a slash
	{
		local( $end ) = &RemovePath( $in );
		push @path, $end;
#		print $in . "\n";
		$in = &RemoveFileName( $in );
	}
	local( $i );
	local( $numelems ) = scalar( @path );
	local( $curpath );
	for( $i = $numelems - 1; $i >= 0; $i-- )
	{
		$curpath .= "/" . $path[$i];
		local( $dir ) = $in . $curpath;
#		print "mkdir $dir\n";
		mkdir $dir, 0777;
	}
}

$blah = shift;
&MakeDirHier( $blah );