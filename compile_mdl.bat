@echo off
set modelname=v_combatshield

if "%~n1" neq "" (
	set modelname=%~n1
)

set VPROJECT=tf2
echo %VPROJECT%\modelsrc\%modelname%\%modelname%.qc
cd %~dp0
bin\studiomdl.exe -game %VPROJECT% %VPROJECT%\modelsrc\%modelname%\%modelname%.qc
pause